#!/bin/bash
# ----------------------------------------------------------------------------------------------------\\
# Description:
#   A basic installer for IBM Cloud Private-CE 1.2.0 on Ubuntu
# ----------------------------------------------------------------------------------------------------\\
# Usage:
#   This assumes all VMs were provisioned to be accessible with the same SSH key
#   All scripts should be run from the master node
#   Edit 00-variables.sh to customize variables, this script will be sourced at the beginning of each script
#   
# ----------------------------------------------------------------------------------------------------\\
# System Requirements:
#   Master and boot nodes on same VM - May have a Public IP and admin IP
#   Proxy on VM
#   Each worker on its VM
#   Ubuntu 16.04.3 on POWER
#   Requires sudo access
# ----------------------------------------------------------------------------------------------------\\
# Docs: (to be checked)
#   Installation Steps From:
#    - https://www.ibm.com/support/knowledgecenter/en/SSBS6K_2.1.0.2/installing/installing.html
#
#
#   Wiki:
#    - https://www.ibm.com/developerworks/community/wikis/home?lang=en#!/wiki/W1559b1be149d_43b0_881e_9783f38faaff
#    - https://www.ibm.com/developerworks/community/wikis/home?lang=en#!/wiki/W1559b1be149d_43b0_881e_9783f38faaff/page/Connect
# ----------------------------------------------------------------------------------------------------\\

# SSH key to use to execute ssh & scp commands from master to slaves
# If you previously set passwordless connection from your workstation to master & slaves,
# it won't be impacted : your public key will remain in master .ssh/authorized_keys
# A different key will be created for ICP password-less SSH ???
export SSH_KEY=~/.ssh/id_rsa
# SSH user to use to execute ssh & scp commands, Admin rights
export SSH_USER=ubuntu

# Domain name for fully qualified host name
export DOMAIN=showroom.boiscolombes.fr.ibm.com

# HTTP Proxy
# If HTTP Proxy is required to access internet
# Mixed case to avoid interferences with shell variables
export HTTP_proxy=x.x.x.x:80

# If you created your cluster within a private network, use the public IP address for the master node to access the cluster
# it will be configured in config.yaml file
# NOT UTILIZED BY THESE SCRIPTS
unset PUBLIC_IP
# export PUBLIC_IP=x.x.x.x

# IP address of master node
# Uncomment it if you don't need it
export MASTER_IP=x.x.x.x

# PROXY node should be on a separate VM for stability
export PROXY_IP=x.x.x.x
export PROXY_HOSTNAME="proxy"

# Worker nodes IP address & hostnames
export WORKERS_IPS=("x.x.x.x" "x.x.x.x" "x.x.x.x")
export WORKERS_HOSTNAMES=("icp-worker-1" "icp-worker-2" "icp-worker-3")


export SSH_KEY=~/.ssh/id_rsa
export SSH_USER=ubuntu
export HTTP_proxy=9.128.135.5:80
export MASTER_IP=9.128.137.55
export PROXY_IP=9.128.137.56
export PROXY_HOSTNAME="icp-proxy"
export WORKERS_IPS=("9.128.137.57" "9.128.137.58")
export WORKERS_HOSTNAMES=("icp-worker1" "icp-worker2")

###################################
### END OF VARIABLES SETTINGS #####
###################################

if [[ "${#WORKERS_IPS[@]}" != "${#WORKERS_HOSTNAMES[@]}" ]]; then
  echo "ERROR: Ensure that the arrays WORKERS_IPS and WORKERS_HOSTNAMES are of the same length"
  return 1
fi
export NUM_WORKERS=${#WORKERS_IPS[@]}

# Create a list of all VMs that are clients of the master node:
SLAVES_IPS=("${WORKERS_IPS[@]}") 
SLAVES_IPS+=(${PROXY_IP})
export SLAVES_IPS
SLAVES_HOSTNAMES=("${WORKERS_HOSTNAMES[@]}") 
#export SLAVES_HOSTNAMES+=$PROXY_HOSTNAME
SLAVES_HOSTNAMES+=(${PROXY_HOSTNAME})
export NUM_SLAVES=${#SLAVES_IPS[@]}


# Manage processor architecture, x86 or ppc64le
export ARCH="$(uname -m)"
if [ "${ARCH}" != "x86_64" ]; then
  export INCEPTION_TAG="-${ARCH}"
fi

# Display summary of configuration
echo
echo "============= CHECK YOUR VARIABLES ================="
echo 
echo "ssh user: $SSH_USER"
echo 
echo "ssh key: ${SSH_KEY}"
echo
echo "HTTP proxy server: $HTTP_proxy"
echo 
echo "Master IP: $MASTER_IP"
echo
echo "Public IP: $PUBLIC_IP" 
echo 
echo "Proxy: ${PROXY_IP}   ${PROXY_HOSTNAME}"
echo 
echo "--- $NUM_WORKERS WORKERS Nodes ---------------------------------"
for ((i=0; i < $NUM_WORKERS; i++)); do
  echo "Worker $i : ${WORKERS_IPS[i]}  ${WORKERS_HOSTNAMES[i]}"
done
echo 
echo "--- ${NUM_SLAVES} SLAVES of MASTER ------------------------------"
for ((i=0; i < $NUM_SLAVES; i++)); do
  echo "Slave $i: ${SLAVES_IPS[i]}  ${SLAVES_HOSTNAMES[i]}"
done
echo

#echo ${WORKERS_HOSTNAMES}
#export ARRAY_IDX=${!WORKERS_IPS[*]}
#for index in $ARRAY_IDX;
#do
#    echo ${WORKERS_IPS[$index]}
#done
