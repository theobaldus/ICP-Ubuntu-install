#!/bin/bash
# User steps for password-less communications

source ./00-variables_ubuntu.sh

echo
echo "Setting up password-less SSH from Master to cluster nodes"
echo "---------------------------------------------------------"
# Generate RSA key on Master node 
echo "==> Generating SSH key in $SSH_KEY"
ssh-keygen -b 4096 -t rsa -f $SSH_KEY -N ""
echo
  
# Move Master public key to slave nodes
echo "==> Moving Master known_hosts file to known_hosts_OLD"
  mv ~/.ssh/known_hosts ~/.ssh/known_hosts_OLD

for ((i=0; i < $NUM_SLAVES; i++)); do
  echo
  echo "==> Working on ${SLAVES_HOSTNAMES[i]} with IP ${SLAVES_IPS[i]}"
  # Prevent SSH identity prompts at first login from Master
  # If slave IP exists in known hosts remove it
  # Retrieve host public key from slaves and add them to known_hosts of Master
  echo "==> Adding ${SLAVES_IPS[i]} host keys to known_hosts of Master"
  ssh-keyscan -H ${SLAVES_IPS[i]} >> ~/.ssh/known_hosts
  ssh-keyscan -H ${SLAVES_HOSTNAMES[i]} >> ~/.ssh/known_hosts
  echo
    # Copy Master public key (Will prompt for password) 
  # to authorized_keys file on workers
  echo "==> Copy Master public key to authorized_keys file on slaves"
  # scp ${SSH_KEY}_pub ${SSH_USER}@${SLAVES_IPS[i]}:~/id_rsa.pub
  # ssh ${SSH_USER}@${SLAVES_IPS[i]} 'mkdir -p ~/.ssh; cat ~/id_rsa.pub | tee -a ~/.ssh/authorized_keys'
  ssh-copy-id ${SSH_USER}@${SLAVES_IPS[i]}
done

# echo IdentityFile ~/.ssh/id_rsa | tee -a ~/.ssh/config
