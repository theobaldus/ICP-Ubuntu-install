#!/bin/bash
# Constructs hosts file on Master and push it to Slaves

# Get the variables
source 00-variables_ubuntu.sh

# Back up old hosts file
sudo cp /etc/hosts /etc/hosts.BEFORE_ICP

echo "127.0.0.1 localhost" | sudo tee /etc/hosts

# echo "fe00::0 ip6-localnet"    | sudo tee -a /etc/hosts
# echo "ff00::0 ip6-mcastprefix" | sudo tee -a /etc/hosts
# echo "ff02::1 ip6-allnodes"    | sudo tee -a /etc/hosts
# echo "ff02::2 ip6-allrouters"  | sudo tee -a /etc/hosts
# echo "ff02::3 ip6-allhosts"    | sudo tee -a /etc/hosts

# Add Slaves to /etc/hosts
echo 
echo "==> Add Slaves to /etc/hosts"
# Loop through the array of Slaves to add them to /etc/hosts
for ((i=0; i < $NUM_SLAVES; i++)); do
  #echo
  #echo "Adding ${SLAVES_IPS[i]} ${SLAVES_HOSTNAMES[i]}"
  echo "${SLAVES_IPS[i]} ${SLAVES_HOSTNAMES[i]} ${SLAVES_HOSTNAMES[i]}.${DOMAIN}" | sudo tee -a /etc/hosts
done

cp /etc/hosts ~/worker-hosts
chown $USER ~/worker-hosts

# echo "$MASTER_IP mycluster.icp" | sudo tee -a /etc/hosts

for ((i=0; i < $NUM_SLAVES; i++)); do
  # Copy over file
  scp ~/worker-hosts  ${SSH_USER}@${SLAVES_HOSTNAMES[i]}:~/worker-hosts

  # Replace worker hosts with file
  ssh  ${SSH_USER}@${SLAVES_HOSTNAMES[i]} 'sudo cp /etc/hosts /etc/hosts.bak; sudo mv ~/worker-hosts /etc/hosts'
done
