# Installation ICP sur POWER pour AI

# Environnement d'installation
## Compute
https://www.ibm.com/support/knowledgecenter/en/SSBS6K_2.1.0/getting_started/architecture.html
VMs: la doc du cours demande 5 VMs au minimum, pas de management node dans cette implémentation :
- bootnode : A boot or bootstrap node is used for running installation, configuration, node scaling, and cluster updates. Only one boot node is required for any cluster. You can use a single node for both master and boot.
- masternode : A master node provides management services and controls the worker nodes in a cluster. Master nodes host processes that are responsible for resource allocation, state maintenance, scheduling, and monitoring.
- proxynode : A proxy node is a node that transmits external request to the services created inside your cluster. 
- workernode1 : A worker node is a node that provides a containerized environment for running tasks. As demands increase, more worker nodes can easily be added to your cluster to improve performance and efficiency.
- workernode2

Cores minimum : 

| Node       | Cores mini | Mem mini |Disk mini|
|------------|:----------:|:--------:|:-------:|
| bootnode   |1           |4         |100GB    |
| masternode |2           |4         |150GB    |
| proxynode  |2           |4         |40GB     |
| workernode1|1           |4         |100GB    |
| workernode2|1           |4         |100GB    |


## Stockage
Filesystems partagés supportés : NFS ou GlusterFS. Pour faire simple, je vais utiliser NFS.
Besoin en stockage sur les nodes : importants ! Le template oVirt crée : 
- un VG de 50 Go
- un LV de 47 Go monté en /
Je dois donc ajouter un deuxième disque de 60 Go ou 110 Go au VG pour atteindre la volumétrie requise. 


## Réseau
Des ports doivent être ouverts, donc désactiver le firewall

## Software
Les versions utilisées par la doc sont : 
- OS : Ubuntu 16.04.3.
- python 2.7.12 dans la doc du cours.
- docker 17.09.1-ce
En utilisant apt-get, j'arrive à ces versions ou > sans problème. 




# Création des VMS

## Réseau
Adresses IP : 
```
icp-boot         9.128.137.55
icp-master       9.128.137.56
icp-proxy        9.128.137.57
icp-worker1      9.128.137.58
icp-worker2      9.128.137.59
```

Mise à jour du DNS.

Mise à jour du Template oVirt pour faciliter la création, je crée une VM puis je la clone ? finalement , non je compte sur cloud-init pour la configuration réseau. https://www.ovirt.org/documentation/vmm-guide/chap-Administrative_Tasks/

### Vérification du fichier de config IP
Il est dans `/etc/network/interfaces.d/50-cloud-init.cfg`, car créé par cloud-init.
Contenu : 
```
# This file is generated from information provided by
# the datasource.  Changes to it will not persist across an instance.
# To disable cloud-init's network configuration capabilities, write a file
# /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg with the following:
# network: {config: disabled}
auto lo
iface lo inet loopback

auto enp0s1
iface enp0s1 inet static
address 9.128.137.56/24
gateway 9.128.137.1
dns-nameservers 9.128.137.131 
dns-search      showroom.boiscolombes.fr.ibm.com
```

### désactiver cloud-init pour le réseau
Après installation et démarrage, désactiver cloud-init pour le réseau en créant le fichier : 
`/etc/cloud/cloud.cfg.d/10_miq_cloud.cfg`
avec pour contenu : 
`network: {config: disabled}`
Sinon, cloud-init modifie la config réseau à chaque redémarrage en supprimant l'interface créée au premier boot. Comme documenté dans : 
$ cat /etc/network/interfaces.d/50-cloud-init.cfg 
```
# This file is generated from information provided by
# the datasource.  Changes to it will not persist across an instance.
# To disable cloud-init's network configuration capabilities, write a file
# /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg with the following:
# network: {config: disabled}
```

### Configuration NTP chrony
les différentes VMs du cluster doivent être synchrones. Configuration du service chrony remplaçant de ntpd :
`sudo apt-get -y install chrony ntpdate`

Fichier /etc/chrony.conf
```
# Use public servers from the pool.ntp.org project.
# Please consider joining the pool (http://www.pool.ntp.org/join.html).
# server 0.centos.pool.ntp.org iburst
server 9.61.37.84 iburst
# Record the rate at which the system clock gains/losses time.
driftfile /var/lib/chrony/drift

# Allow the system clock to be stepped in the first three updates
# if its offset is larger than 1 second.
makestep 1.0 3

# Enable kernel synchronization of the real-time clock (RTC).
rtcsync

# Enable hardware timestamping on all interfaces that support it.
#hwtimestamp *

# Increase the minimum number of selectable sources required to adjust
# the system clock.
#minsources 2

# Allow NTP client access from local network.
#allow 192.168.0.0/16

# Serve time even if not synchronized to a time source.
#local stratum 10

# Specify file containing keys for NTP authentication.
#keyfile /etc/chrony.keys

# Specify directory for log files.
logdir /var/log/chrony

# Select which information is logged.
#log measurements statistics tracking
```
Activation du service
`sudo systemctl restart chrony`
`sudo systemctl enable  chrony`

### Synchronisation si gros décalage : 
`ntpdate ntp.btv.ibm.com`

### Vérifications
Décalage par rapport au serveur : 
```
# chronyc tracking
Reference ID    : 093D2554 (ntp.btv.ibm.com)
Stratum         : 4
Ref time (UTC)  : Mon Jan 22 16:40:22 2018
System time     : 0.000617450 seconds fast of NTP time
Last offset     : +0.000142657 seconds
RMS offset      : 0.005749658 seconds
Frequency       : 1.971 ppm slow
Residual freq   : +0.100 ppm
Skew            : 33.417 ppm
Root delay      : 0.218072146 seconds
Root dispersion : 0.011290949 seconds
Update interval : 65.4 seconds
Leap status     : Normal
```
```
# chronyc sources -v
210 Number of sources = 1

  .-- Source mode  '^' = server, '=' = peer, '#' = local clock.
 / .- Source state '*' = current synced, '+' = combined , '-' = not combined,
| /   '?' = unreachable, 'x' = time may be in error, '~' = time too variable.
||                                                 .- xxxx [ yyyy ] +/- zzzz
||      Reachability register (octal) -.           |  xxxx = adjusted offset,
||      Log2(Polling interval) --.      |          |  yyyy = measured offset,
||                                \     |          |  zzzz = estimated error.
||                                 |    |           \
MS Name/IP address         Stratum Poll Reach LastRx Last sample               
===============================================================================
^* ntp.btv.ibm.com               3   6   377    13  -1579us[-2294us] +/-  132ms
```

### Vérification des pare-feux :
oVirt peut géréner des règles de pare-feux. Vérification que rien ne bloque avec : 
```
$ sudo iptables -L
$ sudo systemctl status firewalld.service
```


## Augmentation de la volumétrie disque
Création de la création de la VM. Avant le premier boot, éditer la VM pour attacher un nouveau disque : Détails de la VM > disks > New
Penser à le créer en thin-provisionning, pour que le data domain ne se remplisse pas trop vite ! Même si sur le V7000U, le LUN est en thin-provisionning & les disques en mode preallocated de oVirt sont en thin-provisionning sur le V7000U.

Redémarrage de la VM, le disque est visible `vdb`

Corriger `/etc/hosts` :
```
$ cat /etc/hosts
127.0.0.1	localhost icp-boot
```


Extension du VG :
`sudo vgextend ovirt-templ-ubuntu-16-vg /dev/vdb`

Vérification :
```
$ sudo vgs
  VG                       #PV #LV #SN Attr   VSize  VFree 
  ovirt-templ-ubuntu-16-vg   2   2   0 wz--n- 66.75g 17.02g
$ sudo pvscan
   PV /dev/vda3   VG ovirt-templ-ubuntu-16-vg   lvm2 [49.75 GiB / 24.00 MiB free]
   PV /dev/vdb    VG ovirt-templ-ubuntu-16-vg   lvm2 [17.00 GiB / 17.00 GiB free]
   Total: 2 [66.75 GiB] / in use: 2 [66.75 GiB] / in no VG: 0 [0   ]
```

Logical Volume à augmenter : `/dev/ovirt-templ-ubuntu-16-vg/root`
```
$ sudo lvdisplay
sudo: unable to resolve host ubuntu23: Connection refused
  --- Logical volume ---
  LV Path                /dev/ovirt-templ-ubuntu-16-vg/root
  LV Name                root
  VG Name                ovirt-templ-ubuntu-16-vg
  LV UUID                cQpw8e-D61i-78Mg-JbOH-I8uT-gPrr-R0LlzU
  LV Write Access        read/write
  LV Creation host, time ovirt-templ-ubuntu-16, 2018-01-15 15:36:46 +0000
  LV Status              available
  # open                 1
  LV Size                47.67 GiB
  Current LE             12203
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           252:0
   
  --- Logical volume ---
  LV Path                /dev/ovirt-templ-ubuntu-16-vg/swap_1
  LV Name                swap_1
  VG Name                ovirt-templ-ubuntu-16-vg
  LV UUID                aUsz2Q-Edb0-qmLW-ucXr-fRkA-iwdR-fKCNpj
  LV Write Access        read/write
  LV Creation host, time ovirt-templ-ubuntu-16, 2018-01-15 15:36:46 +0000
  LV Status              available
  # open                 2
  LV Size                2.06 GiB
  Current LE             527
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           252:1
```
Commande à passer : 
`sudo lvextend -l +100%FREE -r  /dev/ovirt-templ-ubuntu-16-vg/root`

Vérification : 
`df -h`

## Passage des disques en thin-provisionning
zut, j'ai oublié de provisionner les disques en thin-provisionning dans oVirt ! Il faut : 
- créer un 3e disque pour chaque VM, 
- redémarrer la VM,
- le déclarer en PV `pvcreate`
- l'ajouter au VG `vgextend`
- migrer les extends sur le nouveau disque `pvmove`
- surveiller la migration `lvs`
- vérifier que le disque ne contient plus de données `pvs`
- retirer du VG le disque en preallocated `vgreduce`
- supprimer le disque `pvremove`

Processus complet : 
```
$ lsblk
NAME                                    MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sr0                                      11:0    1  1024M  0 rom  
sr1                                      11:1    1   376K  0 rom  
vda                                     253:0    0    50G  0 disk 
├─vda1                                  253:1    0     7M  0 part 
├─vda2                                  253:2    0   244M  0 part /boot
└─vda3                                  253:3    0  49.8G  0 part 
  ├─ovirt--templ--ubuntu--16--vg-root   252:0    0 104.7G  0 lvm  /
  └─ovirt--templ--ubuntu--16--vg-swap_1 252:1    0   2.1G  0 lvm  [SWAP]
vdb                                     253:16   0    57G  0 disk 
└─ovirt--templ--ubuntu--16--vg-root     252:0    0 104.7G  0 lvm  /
vdc                                     253:32   0    57G  0 disk 
ubuntu@icp-worker2:~
$ sudo pvcreate /dev/vdc
  Physical volume "/dev/vdc" successfully created
ubuntu@icp-worker2:~
$ sudo vgextend ovirt-templ-ubuntu-16-vg /dev/vdc
  Volume group "ovirt-templ-ubuntu-16-vg" successfully extended
ubuntu@icp-worker2:~
$ sudo pvmove -b /dev/vdb /dev/vdc
ubuntu@icp-worker2:~
$ sudo lvs -a -o+devices
  LV        VG                       Attr       LSize   Pool Origin Data%  Meta%  Move     Log Cpy%Sync Convert Devices                
  [pvmove0] ovirt-templ-ubuntu-16-vg p-C-aom---  57.00g                           /dev/vdb     3.95             /dev/vdb(0),/dev/vdc(0)
  root      ovirt-templ-ubuntu-16-vg -wI-ao---- 104.69g                                                         /dev/vda3(0)           
  root      ovirt-templ-ubuntu-16-vg -wI-ao---- 104.69g                                                         /dev/vda3(12730)       
  root      ovirt-templ-ubuntu-16-vg -wI-ao---- 104.69g                                                         pvmove0(0)             
  swap_1    ovirt-templ-ubuntu-16-vg -wi-ao----   2.06g                                                         /dev/vda3(12203)       
ubuntu@icp-worker2:~
$ sudo lvs -a -o+devices
  LV     VG                       Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert Devices         
  root   ovirt-templ-ubuntu-16-vg -wi-ao---- 104.69g                                                     /dev/vda3(0)    
  root   ovirt-templ-ubuntu-16-vg -wi-ao---- 104.69g                                                     /dev/vda3(12730)
  root   ovirt-templ-ubuntu-16-vg -wi-ao---- 104.69g                                                     /dev/vdc(0)     
  swap_1 ovirt-templ-ubuntu-16-vg -wi-ao----   2.06g                                                     /dev/vda3(12203)
                                                       /dev/vda3(12203)       
$ sudo pvs -o+pv_used
  PV         VG                       Fmt  Attr PSize  PFree  Used  
  /dev/vda3  ovirt-templ-ubuntu-16-vg lvm2 a--  49.75g     0  49.75g
  /dev/vdb   ovirt-templ-ubuntu-16-vg lvm2 a--  57.00g 57.00g     0 
  /dev/vdc   ovirt-templ-ubuntu-16-vg lvm2 a--  57.00g     0  57.00g
ubuntu@icp-worker2:~
$ sudo vgreduce -f  ovirt-templ-ubuntu-16-vg /dev/vdb
  Removed "/dev/vdb" from volume group "ovirt-templ-ubuntu-16-vg"
                                                   /dev/vda3(12203)
ubuntu@icp-worker2:~
$ lsblk
NAME                                    MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sr0                                      11:0    1  1024M  0 rom  
sr1                                      11:1    1   376K  0 rom  
vda                                     253:0    0    50G  0 disk 
├─vda1                                  253:1    0     7M  0 part 
├─vda2                                  253:2    0   244M  0 part /boot
└─vda3                                  253:3    0  49.8G  0 part 
  ├─ovirt--templ--ubuntu--16--vg-root   252:0    0 104.7G  0 lvm  /
  └─ovirt--templ--ubuntu--16--vg-swap_1 252:1    0   2.1G  0 lvm  [SWAP]
vdb                                     253:16   0    57G  0 disk 
vdc                                     253:32   0    57G  0 disk 
└─ovirt--templ--ubuntu--16--vg-root     252:0    0 104.7G  0 lvm  /

  ```



# Stockage pour persistent storage
ICP supporte les formes de stockage supportés par Kubernetes : NFS, GlusterFS, vSphere Virtual Volume, hostPath pour un cluster mono-noeud. Mes deux options sont donc NFS & GlusterFS.  GlusterFS : filesystem distribué. pas block, pas objet. Pour faire simple dans un premier temps, j'utilise NFS et le serveur x3650 qui sert déjà de serveur NFS. Pour plus tard : https://wiki.centos.org/HowTos/GlusterFSonCentOS


# Préparation du cluster pour l'installation
pas documenté dans le cours, mais dans IBM Knowledge Center.

## Préparation de chaque node
Configurer /etc/hosts de chaque node pour qu'il référence tous les nodes du cluster, y compris lui-même, par son adresse IP et pas par l'adresse de loopback `127.0.0.1`, ni par `127.0.1.1`, à commenter.
```
127.0.0.1    localhost
9.128.137.55 icp-boot       icp-boot.showroom.boiscolombes.fr.ibm.com
9.128.137.56 icp-master   icp-master.showroom.boiscolombes.fr.ibm.com
9.128.137.57 icp-proxy     icp-proxy.showroom.boiscolombes.fr.ibm.com
9.128.137.58 icp-worker1 icp-worker1.showroom.boiscolombes.fr.ibm.com
9.128.137.59 icp-worker2 icp-worker2.showroom.boiscolombes.fr.ibm.com
```
### Master node : max virtual memory areas
```
ubuntu@icp-master:~
$ sudo sysctl -w vm.max_map_count=262144
vm.max_map_count = 262144
```
Pour maintenir au reboot de l'OS : 
`$ sudo nano /etc/sysctl.conf`
ajouter à la fin du fichier : `vm.max_map_count=262144`

### Master node : lower limit of the ephemeral port range
Vérifier la valeur courante de la limite basse des ports éphémères, elle doit être > 10240 : 
```
$ sudo sysctl net.ipv4.ip_local_port_range
net.ipv4.ip_local_port_range = 32768	60999
```
Ici elle est trop basse. L'augmenter à 10240 :
```
$ sudo sysctl -w net.ipv4.ip_local_port_range="10240  60999"
net.ipv4.ip_local_port_range = 10240  60999
```
Pour maintenir au reboot de l'OS : 
`$ sudo nano /etc/sysctl.conf`
ajouter à la fin du fichier : `net.ipv4.ip_local_port_range="10240 60999"`


# Installation de docker
If you want to change the location of the Docker default storage directory, you must configure a bind mount to the new directory before you install IBM Cloud Private. See Specifying a default Docker storage directory by using bind mount.

Attention, j'ai installé docker avant le paramétrage du master node ci dessus.
You must manually install Docker on your boot node. You can either install Docker on the rest of your cluster nodes, or the installer can automatically install Docker on your correctly-configured master, worker, proxy, and optional management nodes.

## Installation sur tous les nodes
J'installe docker sur tous les nodes. Je suis la doc du site docker, en faisant attention à bien choisir les onglets pour ppc64le : https://docs.docker.com/v17.09/engine/installation/linux/docker-ce/ubuntu/#install-docker-ce-1

Commandes à passer :

```
 # sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
  export https_proxy=9.128.135.5:80 ; curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - ; sudo apt-key fingerprint 0EBFCD88 ;  sudo add-apt-repository    "deb [arch=ppc64el] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" ; sudo apt-get update ; sudo apt-get install docker-ce
  python -V
  docker version
```
## Configuration docker sans sudo
A faire sur tous les noeuds du cluster
```
sudo groupadd docker
sudo usermod -aG docker ubuntu ; shutdown -r now
docker run hello-world
```

## Configuration docker pour passer le proxy
A faire sur tous les noeuds du cluster
Créer un répertoire pour la configuration systemd
`sudo mkdir -p /etc/systemd/system/docker.service.d`

Créer un fichier `/etc/systemd/system/docker.service.d/http-proxy.conf` contenant l'adresse du proxy. contenu :

```
[Service]
Environment="HTTP_PROXY=http://9.128.135.5:80/" 
Environment="NO_PROXY=localhost,127.0.0.1,.showroom.boiscolombes.fr.ibm.com,9.128.137.56,9.128.137.57,9.128.137.58,9.128.137.59"

```
Si registre sur une autre machine mais sans proxy, on peut l'ajouter.

Relire la config
`sudo systemctl daemon-reload`

Vérifier la config
```
$ sudo systemctl show --property=Environment docker
Environment=HTTP_PROXY=http://9.128.135.5:80/ NO_PROXY=localhost,127.0.0.1,.showroom.boiscolombes.fr.ibm.com
```

Relancer docker
`sudo systemctl restart docker`

Tester 
`docker run hello-world`

## Configuration pour afficher les logs & services docker dans la console ICP
Set the default logging driver to json-file.
Trouver le logging driver par défaut du démon docker :
```
$ sudo docker info|grep "Logging Driver"
Logging Driver: json-file
```
c'est déjà configuré correctement.

## Option : Configurer le storage driver (pour plus tard)
devicemapper storage driver is the only supported storage driver for Docker EE and Commercially Supported Docker Engine (CS-Engine) on RHEL, CentOS, and Oracle Linux.
Device Mapper is a kernel-based framework that underpins many advanced volume management technologies on Linux. Docker’s devicemapper storage driver leverages the thin provisioning and snapshotting capabilities of this framework for image and container management. This article refers to the Device Mapper storage driver as devicemapper, and the kernel framework as Device Mapper.

## Option : configurer un répertoire de stockage docker différent de /var/lib/docker
https://www.ibm.com/support/knowledgecenter/SSBS6K_2.1.0/installing/docker_dir.html

# Installation du code ICP-CE DEPRECATED - INSTALLER LA VERSION DSX

## Récupérer l'image docker de ICP
 `docker pull ibmcom/icp-inception-ppc64le:latest`

## Créer le répertoire d'installation
 `mkdir /opt/ibm-cloud-private-ce-2.1.0`
`cd /opt/ibm-cloud-private-ce-2.1.0`

## Extraire les fichiers de configuration
```
docker run -e LICENSE=accept \
-v "$(pwd)":/data ibmcom/icp-inception-ppc64le:latest cp \
-r cluster /data
```
# Préparer le déploiment depuis boot-node

## Configurer ssh du bootnode vers les membres du cluster

Pas clair : est-ce que je dois le faire pour root ou pour ubuntu ? Après essais, il apparaît qu'il faut configurer le compte root pour SSH.

### Préparer le compte root pour un acces ssh : 
Créer un mot de passe pour root : `sudo passwd root`
Vérifier : `su`
Autoriser le login ssh pour root, en modifiant la config du démon ssh :
`sudo nano  /etc/ssh/sshd_config`
Changer : `PermitRootLogin prohibit-password` 
pour : `PermitRootLogin yes`
Redémarrer le démon : `sudo systemctl restart sshd`

### Configuration d'une clef publique 
sur boot-node : 

- pour root :

`sudo su -`

créer une clef SSH

`sudo ssh-keygen -b 4096 -t rsa -f /root/.ssh/master.id_rsa -N ""`

Ajouter la clef à la liste des clefs autorisées localement, pour ssh sur localhost sans prompt

`cat /root/.ssh/master.id_rsa.pub | tee –a /root/.ssh/authorized_keys`

Vérifier le login sans prompt avec cette clef, et provoquer l'ajout à .ssh/known_hosts

`ssh -i /root/.ssh/master.id_rsa  root@icp-master`

Remplacer la clef de l'installeur ICP par la nouvelle clef :

`cp ~/.ssh/master.id_rsa /opt/ibm-cloud-private-ce-2.1.0/cluster/ssh_key`

Ajouter la clef publique aux autres serveurs du cluster : 

```
`ssh-copy-id -i /root/.ssh/master.id_rsa.pub root@9.128.137.56`
`ssh-copy-id -i /root/.ssh/master.id_rsa.pub root@9.128.137.57`
`ssh-copy-id -i /root/.ssh/master.id_rsa.pub root@9.128.137.58`
`ssh-copy-id -i /root/.ssh/master.id_rsa.pub root@9.128.137.59`
```

Tester que la clef permet une connexion sans prompt : 

```
`ssh -i /root/.ssh/master.id_rsa 'root@9.128.137.56' 'date'`
`ssh -i /root/.ssh/master.id_rsa 'root@9.128.137.57' 'date'`
`ssh -i /root/.ssh/master.id_rsa 'root@9.128.137.58' 'date'`
`ssh -i /root/.ssh/master.id_rsa 'root@9.128.137.59' 'date'`
```


## Référencer les IP dans cluster/hosts depuis le boot node
le fichier `/<installation_directory>/cluster/hosts` contient les membres du cluster et leurs rôles. Des options supplémentaires sont fournies dans la doc du lab. Ces options `kubelet...` font planter l'installeur... Je les commente.

```
[master]
9.128.137.56
kubelet_extra_args='["--eviction-hard=memory.available<100Mi,node fs.available<2Gi,nodefs.inodesFree<5%","--image-gc-high-threshold=100%", "--image-gc-low-threshold=100%"]'
[worker]
9.128.137.58
9.128.137.59

[proxy]
9.128.137.57
kubelet_extra_args='["--eviction-hard=memory.available<100Mi,node fs.available<2Gi,nodefs.inodesFree<5%", "--image-gc-high-threshold=100%", "--image-gc-low-threshold=100%"]'

#[management]
#4.4.4.4
```

## Remplir le fichier config.yaml

`vim  /opt/ibm-cloud-private-ce-2.1.0/cluster/config.yaml`

Configurer Ansible user

remplacer les lignes, penser à décommenter :

```
calico_ip_autodetection_method: interface=enp0s1
cluster_access_ip: 9.128.137.56
docker_env: ["LimitNOFILE=infinity","LimitNPROC=infinity","LimitCORE=infinity"]

```

Ajouter à la fin du fichier : 

```
# Logging service configuration
logging:
maxAge: 1
storage:
es:
size: 20Gi
path: /opt/ibm/cfc/logging/elasticsearch
ls:
size: 5Gi
path: /opt/ibm/cfc/logging/logstash
kibana:
install: true
```


## Lancer l'installation depuis le boot node

```
cd /opt/ibm-cloud-private-ce-2.1.0/cluster
docker run -e LICENSE=accept --net=host \
-t -v "$(pwd)":/installer/cluster \
ibmcom/icp-inception-ppc64le:latest install | tee install.log
```

Résultat : 
```
PLAY [Checking Python interpreter] ***********************************************************************************************************

TASK [Checking Python interpreter] ***********************************************************************************************************
changed: [9.128.137.56]
changed: [9.128.137.57]
changed: [9.128.137.59]
changed: [9.128.137.58]

PLAY [Checking prerequisites] ****************************************************************************************************************

TASK [Gathering Facts] ***********************************************************************************************************************
ok: [9.128.137.57]
ok: [9.128.137.59]
ok: [9.128.137.56]
ok: [9.128.137.58]

TASK [check : Getting DNS server] ************************************************************************************************************
changed: [9.128.137.59]
changed: [9.128.137.57]
changed: [9.128.137.56]
changed: [9.128.137.58]
...

$ cat install.log
 [WARNING]: While constructing a mapping from /installer/cluster/config.yaml,
line 9, column 1, found a duplicate dict key (size). Using last defined value
only.
 [WARNING]: While constructing a mapping from /installer/cluster/config.yaml,
ok: [9.128.137.57] => (item=ip6_tables)
ok: [9.128.137.59] => (item=ip_set)
ok: [9.128.137.56] => (item=ip_set)
ok: [9.128.137.57] => (item=ip_set)
ok: [9.128.137.59] => (item=x_tables)
ok: [9.128.137.57] => (item=x_tables)
ok: [9.128.137.56] => (item=x_tables)
ok: [9.128.137.59] => (item=xt_mark)
ok: [9.128.137.56] => (item=xt_mark)
ok: [9.128.137.57] => (item=xt_mark)
ok: [9.128.137.59] => (item=xt_addrtype)
ok: [9.128.137.58] => (item=ip6_tables)
ok: [9.128.137.57] => (item=xt_addrtype)
ok: [9.128.137.56] => (item=xt_addrtype)
ok: [9.128.137.59] => (item=xt_multiport)
ok: [9.128.137.57] => (item=xt_multiport)
ok: [9.128.137.56] => (item=xt_multiport)
ok: [9.128.137.59] => (item=nfnetlink)
ok: [9.128.137.57] => (item=nfnetlink)
ok: [9.128.137.56] => (item=nfnetlink)
ok: [9.128.137.58] => (item=ip_set)
ok: [9.128.137.59] => (item=ipip)
ok: [9.128.137.57] => (item=ipip)
ok: [9.128.137.56] => (item=ipip)
ok: [9.128.137.59] => (item=ip_tunnel)
ok: [9.128.137.57] => (item=ip_tunnel)
ok: [9.128.137.56] => (item=ip_tunnel)
ok: [9.128.137.59] => (item=tunnel4)
ok: [9.128.137.57] => (item=tunnel4)
ok: [9.128.137.56] => (item=tunnel4)
ok: [9.128.137.58] => (item=x_tables)
ok: [9.128.137.58] => (item=xt_mark)
ok: [9.128.137.58] => (item=xt_addrtype)
ok: [9.128.137.58] => (item=xt_multiport)
ok: [9.128.137.58] => (item=nfnetlink)
ok: [9.128.137.58] => (item=ipip)
ok: [9.128.137.58] => (item=ip_tunnel)
ok: [9.128.137.58] => (item=tunnel4)
TASK [check : Calico Validation - Making kernel modules persistent across reboot] ***
ok: [9.128.137.56] => (item=ip_tables)
ok: [9.128.137.59] => (item=ip_tables)
ok: [9.128.137.57] => (item=ip_tables)
ok: [9.128.137.58] => (item=ip_tables)
ok: [9.128.137.59] => (item=ip6_tables)
ok: [9.128.137.57] => (item=ip6_tables)
ok: [9.128.137.56] => (item=ip6_tables)
ok: [9.128.137.59] => (item=ip_set)
ok: [9.128.137.57] => (item=ip_set)
ok: [9.128.137.56] => (item=ip_set)
ok: [9.128.137.59] => (item=x_tables)
ok: [9.128.137.57] => (item=x_tables)
ok: [9.128.137.58] => (item=ip6_tables)
ok: [9.128.137.56] => (item=x_tables)
ok: [9.128.137.59] => (item=xt_mark)
ok: [9.128.137.57] => (item=xt_mark)
ok: [9.128.137.56] => (item=xt_mark)
ok: [9.128.137.59] => (item=xt_addrtype)
ok: [9.128.137.57] => (item=xt_addrtype)
ok: [9.128.137.56] => (item=xt_addrtype)
ok: [9.128.137.59] => (item=xt_multiport)
ok: [9.128.137.57] => (item=xt_multiport)
ok: [9.128.137.56] => (item=xt_multiport)
ok: [9.128.137.59] => (item=nfnetlink)
ok: [9.128.137.57] => (item=nfnetlink)
ok: [9.128.137.56] => (item=nfnetlink)
ok: [9.128.137.58] => (item=ip_set)
ok: [9.128.137.59] => (item=ipip)
ok: [9.128.137.57] => (item=ipip)
ok: [9.128.137.56] => (item=ipip)
ok: [9.128.137.57] => (item=ip_tunnel)
ok: [9.128.137.59] => (item=ip_tunnel)
ok: [9.128.137.56] => (item=ip_tunnel)
ok: [9.128.137.58] => (item=x_tables)
ok: [9.128.137.59] => (item=tunnel4)
ok: [9.128.137.57] => (item=tunnel4)
ok: [9.128.137.56] => (item=tunnel4)
ok: [9.128.137.58] => (item=xt_mark)
ok: [9.128.137.58] => (item=ipip)
ok: [9.128.137.58] => (item=ip_tunnel)
ok: [9.128.137.58] => (item=tunnel4)
TASK [check : Getting ufw status] **********************************************
changed: [9.128.137.57]
changed: [9.128.137.56]
changed: [9.128.137.59]
changed: [9.128.137.58]
PLAY [Installing Docker Runtime] ***********************************************
TASK [Gathering Facts] *********************************************************
ok: [9.128.137.56]
ok: [9.128.137.59]
ok: [9.128.137.57]
ok: [9.128.137.58]
TASK [docker-engine : Getting Docker engine version] ***************************
changed: [9.128.137.57]
changed: [9.128.137.56]
changed: [9.128.137.59]
changed: [9.128.137.58]
TASK [docker-engine : Setting docker version variable] *************************
ok: [9.128.137.58]
ok: [9.128.137.59]
ok: [9.128.137.56]
ok: [9.128.137.57]
PLAY [Checking master node] ****************************************************
TASK [Fetching kubelet service status] *****************************************
ok: [9.128.137.56]
PLAY [Checking local work directory] *******************************************
TASK [Getting installer directory] *********************************************
ok: [localhost]
TASK [Checking work directory] *************************************************
changed: [localhost]
TASK [Validating work directory] ***********************************************
fatal: [localhost]: FAILED! => {"failed": true, "msg": "The conditional check 'path.stdout != \"\"' failed. The error was: error while evaluating conditional (path.stdout != \"\"): 'ansible.parsing.yaml.objects.AnsibleUnicode object' has no attribute 'stdout'\n\nThe error appears to have been in '/installer/playbook/reinstall-precheck.yaml': line 38, column 7, but may\nbe elsewhere in the file depending on the exact syntax problem.\n\nThe offending line appears to be:\n\n\n    - name: Validating work directory\n      ^ here\n"}
PLAY RECAP *********************************************************************
9.128.137.56               : ok=25   changed=7    unreachable=0    failed=0   
9.128.137.57               : ok=24   changed=7    unreachable=0    failed=0   
9.128.137.58               : ok=24   changed=7    unreachable=0    failed=0   
9.128.137.59               : ok=24   changed=7    unreachable=0    failed=0   
localhost                  : ok=2    changed=1    unreachable=0    failed=1   
Playbook run took 0 days, 0 hours, 3 minutes, 56 seconds

```

# Faire le ménage avant réinstall ? 
```
docker images 
docker rmi e615f4f30f25 e615f4f30f25 ff4bb0dc2294
docker ps -a 
docker rm -f e816e023ef9d ea530f09979a be40056b48ac e7e28680b797 b4e8eb172dcb 009c2d0b3e34 4c9b6d8a6bc4 b4e8eb172dcb 26b71b21b41b b0d263c8b717 5f098e3acb16 6c2a69d4ac53 05966a9fc7af 2eda64baefcd 3302caa19701 c2c50dc43437 71d190fe18e7 289f871f0e60 739d129bd865 e5638d1f8e95 8c7ffd7c96d0 32934d879305 
docker ps -a 
docker rmi e615f4f30f25 e615f4f30f25 ff4bb0dc2294
docker rmi -f e615f4f30f25 e615f4f30f25 ff4bb0dc2294
docker images 
```
Il faut peut-etre aussi faire un nettoyage de répertoires, puisque l'erreur apparait dans `/installer/playbook/reinstall-precheck.yaml`.
 - copier les fichiers de config, hosts, ssh_key pour les sauvegarder.

```
$ cd /opt/ibm-cloud-private-ce-2.1.0/cluster
$ sudo cp config.yaml hosts ssh_key ~
```

 - supprimer le répertoire `/opt/ibm-cloud-private-ce-2.1.0/cluster`
et refaire l'installation.

Importer l'installeur
```
/opt/ibm-cloud-private-ce-2.1.0
$ docker run -e LICENSE=accept \
> -v "$(pwd)":/data ibmcom/icp-inception-ppc64le:latest cp \
> -r cluster /data
```

Importer les fichiers de config sauvegardés : 
```
$ cd cluster/
$ sudo cp ~/config.yaml ~/hosts ~/ssh_key .
```

Relancer l'installation. Erreur toujours présente...

# Installation avec le boot node sur le master node
pour simplifier, j'installe avec un boot node sur le master node, et le management node avec. donc le fichier `cluster/hosts` devient : 

```
[master]
9.128.137.56
# kubelet_extra_args='["--eviction-hard=memory.available<100Mi,node fs.available<2Gi,nodefs.inodesFree<5%", "--image-gc-high-threshold=100%", "--image-gc-low-threshold=100%"]'

[worker]
9.128.137.58
9.128.137.59

[proxy]
9.128.137.57
# kubelet_extra_args='["--eviction-hard=memory.available<100Mi,node fs.available<2Gi,nodefs.inodesFree<5%", "--image-gc-high-threshold=100%", "--image-gc-low-threshold=100%"]'

[management]
9.128.137.56
```

## Configuration SSH
faite par script 01-1...

## Configuration /etc/hosts


Fichier hosts de Sébastien : 

~~~
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
10.7.19.173 seb173

::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.7.19.173 seb173 seb173.localdomain
10.7.19.173 mycluster.icp
~~~
